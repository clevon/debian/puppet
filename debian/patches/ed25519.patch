From ddbb8dbad08337c6d4954b69982a5ec11f65085f Mon Sep 17 00:00:00 2001
From: Raul Tambre <raul.tambre@cleveron.com>
Date: Sun, 30 Jan 2022 19:39:04 +0200
Subject: [PATCH] (PUP-11439) Support Ed25519 keys/certificates

The generic interface usage was added by 78712feb5dd54565d6a86be341410b0c3e04aab6, which improved key format support.
ruby-openssl 3.0, shipped in Ruby 3.0, supports Ed25519 keys using the generic interface and returns a OpenSSL::PKey::PKey.
The only thing preventing these from working is a simple type check. Update it to only check key types that aren't supported.
---
 lib/puppet/ssl/ssl_provider.rb   | 6 +++---
 lib/puppet/x509/cert_provider.rb | 6 +++---
 2 files changed, 6 insertions(+), 6 deletions(-)

--- a/lib/puppet/ssl/ssl_provider.rb
+++ b/lib/puppet/ssl/ssl_provider.rb
@@ -134,7 +134,7 @@ class Puppet::SSL::SSLProvider
   #
   # @param cacerts [Array<OpenSSL::X509::Certificate>] Array of trusted CA certs
   # @param crls [Array<OpenSSL::X509::CRL>] Array of CRLs
-  # @param private_key [OpenSSL::PKey::RSA, OpenSSL::PKey::EC] client's private key
+  # @param private_key [OpenSSL::PKey::PKey] client's private key
   # @param client_cert [OpenSSL::X509::Certificate] client's cert whose public
   #   key matches the `private_key`
   # @param revocation [:chain, :leaf, false] revocation mode
@@ -199,7 +199,7 @@ class Puppet::SSL::SSLProvider
   # of the private key, and that it hasn't been tampered with since.
   #
   # @param csr [OpenSSL::X509::Request] certificate signing request
-  # @param public_key [OpenSSL::PKey::RSA, OpenSSL::PKey::EC] public key
+  # @param public_key [OpenSSL::PKey::PKey] public key
   # @raise [Puppet::SSL:SSLError] The private_key for the given `public_key` was
   #   not used to sign the CSR.
   # @api private
@@ -281,7 +281,7 @@ class Puppet::SSL::SSLProvider
   def resolve_client_chain(store, client_cert, private_key)
     client_chain = verify_cert_with_store(store, client_cert)
 
-    if !private_key.is_a?(OpenSSL::PKey::RSA) && !private_key.is_a?(OpenSSL::PKey::EC)
+    if private_key.is_a?(OpenSSL::PKey::DSA)
       raise Puppet::SSL::SSLError, _("Unsupported key '%{type}'") % { type: private_key.class.name }
     end
 
--- a/lib/puppet/x509/cert_provider.rb
+++ b/lib/puppet/x509/cert_provider.rb
@@ -176,7 +176,7 @@ class Puppet::X509::CertProvider
   # historical reasons, names are case insensitive.
   #
   # @param name [String] The private key identity
-  # @param key [OpenSSL::PKey::RSA] private key
+  # @param key [OpenSSL::PKey::PKey] private key
   # @param password [String, nil] If non-nil, derive an encryption key
   #   from the password, and use that to encrypt the private key. If nil,
   #   save the private key unencrypted.
@@ -227,7 +227,7 @@ class Puppet::X509::CertProvider
   # @param password [String, nil] If the private key is encrypted, decrypt
   #   it using the password. If the key is encrypted, but a password is
   #   not specified, then the key cannot be loaded.
-  # @return [OpenSSL::PKey::RSA, OpenSSL::PKey::EC] The private key
+  # @return [OpenSSL::PKey::PKey] The private key
   # @raise [OpenSSL::PKey::PKeyError] The `pem` text does not contain a valid key
   #
   # @api private
@@ -299,7 +299,7 @@ class Puppet::X509::CertProvider
   # Create a certificate signing request (CSR).
   #
   # @param name [String] the request identity
-  # @param private_key [OpenSSL::PKey::RSA] private key
+  # @param private_key [OpenSSL::PKey::PKey] private key
   # @return [Puppet::X509::Request] The request
   #
   # @api private
